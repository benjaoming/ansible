#!/bin/sh

BASE_CMD="ansible-playbook playbook.yml --ask-vault-pass"

if [ -z "$1" ]; then
    echo "Deploying all!"
    $BASE_CMD
else
    case $1 in
        "services")
            if [ -z "$2" ]; then
                echo "Deploying all services!"
                $BASE_CMD --tags setup_services
            else
                echo "Deploying services: $2"
                $BASE_CMD --tags setup_services --extra-vars "services=$2"
            fi
    esac
fi
